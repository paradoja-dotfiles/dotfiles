{
  # config,
  pkgs,
  # flake-utils,
  ...
}:

{
  imports = [
    ../modules/editor.nix # .doom.d is managed separately
    ../modules/git
    ../modules/tech
    ../modules/localBin
    ../modules/nixUtils.nix
    ../modules/shell # fish - some fish conf comes from other packages
    ../modules/tmux
    ../modules/network
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.packages =
    with pkgs;
    let
      system = "x86_64-linux";
      pkgs-prev = import (fetchFromGitHub {
        owner = "NixOS";
        repo = "nixpkgs";
        rev = "24.11";
        hash = "sha256-btHN1czJ6rzteeCuE/PNrdssqYD2nIA4w48miQAFloM=";
      }) { inherit system; };
    in
    [
      nixVersions.latest

      gnupg # To make sure we have an updated gpg for pass
      pass

      hledger
      hledger-ui
      hledger-web
      solaar
      # tree # TODO
      cmake # vterm requires cmake
      fd
      fortune
      glances
      graphviz
      haskellPackages.yeganesh # to new desktop module
      imagemagick
      killall
      pandoc
      pdfarranger
      # pdfmixtool
      ripgrep
      sqlite
      unzip
      xdg-utils
      wl-clipboard
      # TODO update!
      pkgs-prev.zotero
      # percollate # Add to app directly that uses it TODO

      enchant # TODO needs wrapper or something as it doesn't access dicts
      (aspellWithDicts (
        ps: with ps; [
          es
          de
          en
          pt_BR
          nl
          pl
        ]
      ))
      (hunspellWithDicts (
        with pkgs.hunspellDicts;
        [
          es
          es_ES
          de_DE
          en_US
          en_GB-ise
          nl_NL
        ]
      ))
    ];

  home.file = {
    ".profile".source = ./.profile;
  };

  xdg.enable = true; # TODO: move other config files to xdg
  xdg.configFile."nix/nix.conf".text = ''
    experimental-features = nix-command flakes
    substituters = https://cache.nixos.org https://nix-community.cachix.org https://typelevel.cachix.org https://digitallyinduced.cachix.org https://haskell-language-server.cachix.org https://helix.cachix.org
    trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs= typelevel.cachix.org-1:UnD9fMAIpeWfeil1V/xWUZa2g758ZHk8DvGCd/keAkg= digitallyinduced.cachix.org-1:y+wQvrnxQ+PdEsCt91rmvv39qRCYzEgGQaldK26hCKE= haskell-language-server.cachix.org-1:juFfHrwkOxqIOZShtC4YC1uT1bBcq2RSvC7OMKx0Nz8= helix.cachix.org-1:ejp9KQpR1FBI2onstMQ34yogDm4OgU2ru6lIwPvuCVs=
  '';

  programs = {
    atuin = {
      enable = true;
      # TODO test
      enableFishIntegration = false;
    };

    bat.enable = true;

    bottom.enable = true; # system monitor btm

    broot = {
      # interactive tree
      enable = true;
      enableFishIntegration = true;
      settings.modal = true;
      settings.verbs = [
        {
          invocation = "p";
          execution = ":parent";
        }
        {
          invocation = "edit";
          shortcut = "e";
          execution = "ep {file}";
        }
        {
          invocation = "create {subpath}";
          execution = "$EDITOR {directory}/{subpath}";
        }
        {
          invocation = "view";
          execution = "bat {file}";
        }
      ];
    };

    direnv = {
      enable = true;
    };

    fzf = {
      enable = true;
      # TODO Test atuin?
      enableFishIntegration = true;
    };

    htop = {
      enable = true;
    };

    jq = {
      enable = true;
    };

    topgrade = {
      # TODO Not flexible enough - can't fix order
      enable = true;
      settings = {
        misc = {
          # Don't ask for confirmations
          assume_yes = true;

          # Disable specific steps - same options as the command line flag
          # TODO system should be enabled for non-nixos systems
          disable = [
            "home_manager"
            "vim"
            "nix"
            "gnome_shell_extensions"
          ]; # this we manage ourselves

          # repos = [ ~/org ~/.dotfiles ~/.doom.d ];

          cleanup = true;
        };

        commands = {
          "Update system" = "porfi sync-system";
          # "Run garbage collection on Nix store" = "nix-collect-garbage";
        };
      };
    };
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "24.11";
}
