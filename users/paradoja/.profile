if [ -f "$HOME/.nix-profile/etc/profile.d/nix.sh" ];
then
    . "$HOME/.nix-profile/etc/profile.d/nix.sh"
fi
export PATH="$HOME/bin:$HOME/.nix-profile/bin:$PATH"
export XDG_DATA_DIRS="$HOME/.nix-profile/share:$XDG_DATA_DIRS"
