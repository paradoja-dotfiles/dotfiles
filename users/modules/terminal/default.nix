{ ... }:

{
  xdg.configFile = {
    "./alacritty" = {
      source = ./alacritty;
      recursive = true;
    };
  };

  home.file = {
    "bin/term-switch" = {
      source = ./term-switch;
    };

    ".config/fish/completions/term-switch.fish".source = ./term-switch.fish;
  };

  programs = {
    # Alacritty is managed by the home system on purpose
    # alacritty = {
    #   enable = true;
    # };
  };
}
