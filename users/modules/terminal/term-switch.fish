#!/usr/bin/env fish
# term-fish fish completions

# TODO automate reading theme names?
# set -l commands apply-system apply-users dist-upgrade sync-system update-deps
complete -c term-switch -f
complete -c term-switch -a default -d "Default - remove theme info"
complete -c term-switch -a doom-one -d "Doom one"
complete -c term-switch -a solarized-dark -d "Solarized dark"
complete -c term-switch -a solarized-light -d "Solarized light"
complete -c term-switch -a material -d Material
complete -c term-switch -a one-dark -d "One dark"
complete -c term-switch -a one-light -d "One light"
