#!/usr/bin/env fish

# only run in WSL
if count (grep microsoft /proc/version) >/dev/null
    set -g DISPLAY (awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
    set -g LIBGL_ALWAYS_INDIRECT 1

    function emacs
        set -l em (which emacs)
        DISPLAY=$DISPLAY $em -d $DISPLAY $argv
    end

    function setemacs
        set -l em (which emacs)
        DISPLAY=$DISPLAY $em -d $DISPLAY $argv &
    end
end
