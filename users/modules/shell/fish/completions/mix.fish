#!/usr/bin/env fish

# mix fish completions
# TODO cache?

# $fish_mix_completions_local_cache can be relative
function __fish_mix_completions_setup_cache
    if not test -e $fish_mix_completions_local_cache
        mkdir -p (dirname $fish_mix_completions_local_cache)
        mix help >$fish_mix_completions_local_cache 2>/dev/null
    end
end

function __fish_mix_completions
    if type -q mix
        if set -q fish_mix_completions_local_cache
            __fish_mix_completions_setup_cache
            set listing (cat $fish_mix_completions_local_cache | string split0)
        else
            set listing (mix help | string split0)
        end
        for line in (printf $listing | tail -n +2 | grep ^mix)
            set -l command (echo $line | cut -d' ' -f2)
            set -l description (echo $line | cut -d'#' -f2 | string trim)
            echo -e "$command\t$description"
        end
    end
end

complete -f -c mix -a "(__fish_mix_completions)"
