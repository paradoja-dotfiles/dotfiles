#!/usr/bin/env fish

# Moar functions
function set-global -a variable value
    contains $variable $value
    or set -gx $variable $value
end

function add-global-path -a path
    contains $path $fish_user_paths
    or set -gax fish_user_paths $path
end

# TODO Path can't depend on variable right now, as this is a function, not a
# macro
function add-env-if -a variable file path
    if test -e $file
        set -q $variable[1]
        or set -gax $variable $file
        if set -q path[1]
            set -gax fish_user_paths $path
        end
    end
end

function add-path-if -a path
    if test -e $path
        set -gax fish_user_paths $path
    end
end

# Android Studio
add-path-if "$HOME/opt/android-studio/bin"
set androidRoot "$HOME/Android/Sdk"
add-env-if ANDROID_SDK_ROOT $androidRoot
add-path-if "$androidRoot/emulator"
add-path-if "$androidRoot/tools"
add-path-if "$androidRoot/platform-tools"
set --erase androidRoot

# ASDF
set asdf_path "$HOME/.asdf/asdf.fish"
if test -e $asdf_path
    . $asdf_path
    direnv hook fish | source
    # run once to install completions
    # mkdir -p ~/.config/fish/completions; and ln -s ~/.asdf/completions/asdf.fish ~/.config/fish/completions
end

# Go
add-env-if GOROOT "$HOME/opt/go" "$HOME/opt/go/bin"
add-env-if GOPATH "$HOME/opt/gopath" "$HOME/go/gopath/bin"
if set -q GOPATH
    add-env-if GOBIN "$GOPATH/bin" "$GOPATH/bin"
end

# Haskell
set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME
test -f $HOME/.ghcup/env; and set -gx PATH $HOME/.cabal/bin $HOME/.ghcup/bin $PATH

# Rust with rustup
add-path-if "$HOME/.cargo/bin"

# Elixir
add-path-if "$HOME/opt/elixir-ls"
if type -fq iex # we assume elixir is installed if iex exists
    set ERL_AFLAGS "-kernel shell_history enabled"
end

# Swift
add-path-if "$HOME/opt/swift/usr/bin"

# Ocaml/ReasonMl
# The following, bash commented code -- the suggested one -- prepends, no
# matter what, the ocaml directory to the path. That's why it's not
# used and we do it manually bellow.
# local opam_init=$HOME/.opam/opam-init/init.zsh
# [[ -a $opam_init ]] &&
#     . $opam_init > /dev/null 2> /dev/null || true
if type -q opam
    add-global-path (opam config var bin)
    eval (opam env | grep -v \^"set -gx PATH")
end

# # nodejs
# add-env-if NVM_DIR "$HOME/.nvm"
# set -l nvm_init "$NVM_DIR/nvm.sh"

if test -e $nvm_init
    function nvm
        bass source ~/.nvm/nvm.sh ';' nvm $argv
    end
end
add-path-if "$HOME/.config/yarn/global/node_modules/.bin"
add-path-if "$HOME/.yarn/bin"

# Scala + SBT
set SBT_OPTS "-Xms512M -Xmx1536M -Xss1M -XX:+CMSClassUnloadingEnabled"
alias amm="amm --no-remote-logging"

# Lua && luarocks
add-path-if "$HOME/.luarocks/bin"



#### Conclusion
functions -e set-global add-global-path add-env-if add-path-if
