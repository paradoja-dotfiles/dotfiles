#!/usr/bin/env fish

# functions
function inNixOS
    grep -q NAME=NixOS /etc/os-release
end

function set-universal -a variable value
    contains $variable $value
    or set -U $variable $value
end

function prepend-path -a path
    contains $path $fish_user_paths
    or set -gpx fish_user_paths $path
end

function toggle_bindings
    if test "$fish_key_bindings" = fish_default_key_bindings
        fish_vi_key_bindings
    else
        fish_default_key_bindings
    end
    commandline -f repaint
end

function setup
    bind \cz toggle_bindings
    bind -M insert \cz toggle_bindings
    # For now these don't work, so the previous fish_mode_prompt fixes it
    # set fish_cursor_default block
    # set fish_cursor_insert line
    # set fish_cursor_replace_one underscore
    # set fish_cursor_visual block
    if not set -q INSIDE_EMACS
        fish_vi_key_bindings # default are vi out of emacs/doom
    else
        fish_default_key_bindings
    end
    # in the pager search with C-k/j
    bind -M insert \ck 'if commandline --paging-mod e; up-or-search; end'
    bind -M insert \cj 'if commandline --paging-mode; down-or-search; end'
    bind -M insert \ch 'if commandline --paging-mode; commandline --function backward-char; end'
    bind -M insert \cl 'if commandline --paging-mode; commandline --function forward-char; end'
end

setup
prepend-path $HOME/bin

set-universal fish_greeting ""

#################
# body

alias ncal="ncal -M"

set -x DIRENV_LOG_FORMAT "" # Also a bit added in nixFish.fish
set -x EDITOR ep

source $HOME/.config/fish/nixFish.fish

if not inNixOS
    if test -z "$NIX_SET"
        if test -e $HOME/.nix-profile/etc/profile.d/nix.sh
            set nixProfile $HOME/.nix-profile/etc/profile.d/nix.sh
        else if test -e /etc/profile.d/nix.sh
            set nixProfile /etc/profile.d/nix.sh
        else
            echo 'Can\'t set nix' >&2
        end

        if set -q nixProfile
            set -x NIX_SET 1
            bass source $nixProfile
            set -x NIX_PAGER cat
        end
    end

    # this has to happen after sourcing the nix profile
    set -x NIX_PATH $HOME/.nix-defexpr/channels $NIX_PATH

    source $HOME/.config/fish/configPaths.fish
    source $HOME/.config/fish/windowsX.fish
    setupNixXdgDataDir
end

# TODO Set editors
# set -g EDITOR ec
# set -g VISUAL ecw

#################
# Conclusion
functions -e setup set-universal prepend-path
