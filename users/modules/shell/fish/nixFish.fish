#!/usr/bin/env fish

function setupNixXdgDataDir
    set -l nixShare ~/.nix-profile/share
    if test -z "$XDG_DATA_DIRS"
        set --path -gx XDG_DATA_DIRS "/usr/local/share:/usr/share"
    end
    if test -e $nixShare && not contains $nixShare $XDG_DATA_DIRS
        set --path -gx XDG_DATA_DIRS $XDG_DATA_DIRS $nixShare
    end
end

# We hack the standard vcs_prompt to print info about the nix-shell and direv
function fish_vcs_prompt --description 'Print the prompts for all available vcsen'
    set -l nix_shell_info (if test -n "$IN_NIX_SHELL"; echo -n " {ns}"; end)
    set -l direnv_info (if test -n "$DIRENV_DIR"; echo -n "!"; end)
    echo -n -s "$nix_shell_info"
    echo -n -s "$direnv_info"
    fish_git_prompt $argv
    or fish_hg_prompt $argv
end

function shHaskell --description 'start a nix-shell with the given Haskell packages'
    nix-shell --run fish -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [ $argv ])"
end

function shRuby --description 'start a nix-shell with the given Ruby packages'
    nix-shell --run fish -p "with pkgs.rubyPackages_3_3; [ ruby_3_3 $argv ]"
end

function shPython3 --description 'start a nix-shell with the given python 3 packages'
    nix-shell --run fish -p "with pkgs.python311Packages; [ python3 $argv ]"
end

alias shPython="shPython3"

function shPython2 --description 'start a nix-shell with the given python 2 packages'
    nix-shell --run fish -p "with pkgs.python2Packages; [ python2 $argv ]"
end

function nxsh --description 'start a nix-shell with the given general packages'
    nix-shell --run fish -p "with pkgs; [ $argv ]"
end
