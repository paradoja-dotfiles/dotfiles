#!/usr/bin/env fish

function fish_mode_prompt
    if test "$fish_key_bindings" = fish_default_key_bindings
        return
    end
    switch $fish_bind_mode
        case default
            echo -en "\e[2 q"
            set_color -o brred
            echo "[N]"
        case insert
            echo -en "\e[6 q"
            set_color -o brgreen
            echo "[I]"
        case replace_one
            echo -en "\e[4 q"
            set_color -o bryellow
            echo "[R]"
        case visual
            echo -en "\e[2 q"
            set_color -o brmagenta
            echo "[V]"
        case '*'
            echo -en "\e[2 q"
            set_color -o brred
            echo "[?]"
    end
    echo " "
    set_color normal
end
