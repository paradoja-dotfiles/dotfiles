#!/usr/bin/env fish

function set-ssh --description "Set ssh agent"
    set -l agent_script "$XDG_CACHE_HOME/ssh-agent-fish-script"
    set -l run_add fish -c "ssh-add -l &>/dev/null"

    $run_add
    if test $status = 2
        if test -r $agent_script
            eval (cat $agent_script) &>/dev/null
        end

        $run_add
        if test $status = 2
            fish -c "umask 066; ssh-agent -c > $agent_script"
            eval (cat $agent_script) &>/dev/null
        end
    end

    $run_add
    if test $status = 1 # no identities
        ssh-add &>/dev/null # -t 1h
    end
end
