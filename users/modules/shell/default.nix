{ config, pkgs, ... }:

{
  # xdg.configFile."fish." = { ...
  # doesn't work as `fish` here is created and managed by home-manager
  # independently of us setting this, and won't write to that directory.
  # `home.file.".config/..."`, will, in turn, work, though.
  home.file.".config/fish" = {
    source = ./fish;
    recursive = true;
  };

  programs = {
    fish = {
      enable = true;
      shellAliases = {
        doom = "~/.config/emacs/bin/doom"; # Assumes doom emacs is installed here
      };
      interactiveShellInit = "source ~/.config/fish/baseConfig.fish";
      # TODO automate plugin updates
      plugins = [
        {
          name = "z";
          # nurl https://github.com/jethrokuan/z master
          src = pkgs.fetchFromGitHub {
            owner = "jethrokuan";
            repo = "z";
            rev = "master";
            hash = "sha256-+FUBM7CodtZrYKqU542fQD+ZDGrd2438trKM0tIESs0=";
          };
        }
        {
          name = "bass";
          # nurl https://github.com/edc/bass master
          src = pkgs.fetchFromGitHub {
            owner = "edc";
            repo = "bass";
            rev = "master";
            hash = "sha256-3d/qL+hovNA4VMWZ0n1L+dSM1lcz7P5CQJyy+/8exTc=";
          };
        }
      ];
    };
  };
}
