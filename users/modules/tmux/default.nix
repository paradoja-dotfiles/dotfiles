{ config, pkgs, ... }:

{
  xdg.configFile."tmux/tmuxBaseConf.conf".source = ./tmux.conf;

  programs = {
    tmux = {
      enable = true;
      clock24 = true; # I don't use it but set it just in case
      # config on home.file
      extraConfig = "source-file ~/.config/tmux/tmuxBaseConf.conf";
    };
  };
}
