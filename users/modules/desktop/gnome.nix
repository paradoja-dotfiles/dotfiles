{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    wofi # currently rofi won't work in Gnome Wayland
  ];

  home.file = {
    # Gnome user image
    ".face".source = ./files/abby-con-bici-cropped.jpg;
  };
}
