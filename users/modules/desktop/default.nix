{
  config,
  pkgs,
  lib,
  ...
}:
let
  nixGLWrap =
    pkg:
    pkgs.runCommand "${pkg.name}-nixgl-wrapper" { } ''
      mkdir $out
      ln -s ${pkg}/* $out
      rm $out/bin
      mkdir $out/bin
      for bin in ${pkg}/bin/*; do
       wrapped_bin=$out/bin/$(basename $bin)
       echo "exec ${lib.getExe pkgs.nixgl.nixGLIntel} $bin \$@" > $wrapped_bin
       chmod +x $wrapped_bin
      done
    '';
in
{
  imports = [
    ./gnome.nix
    ../terminal # alacritty
    # ./xmonad.nix # TODO do something with this
  ];

  home.packages = with pkgs; [
    xdotool

    (nixGLWrap calibre)
    brightnessctl
    xclip
    zathura

    # evince
    flameshot

  ];

  xdg.configFile = {
    "rofi/desktop-switch".source = ./files/desktop-switch;
    "rofi/desktop-switcher" = {
      source = ./files/desktop-switcher;
      recursive = true;
    };
    "rofi/arthur.rasi".source = ./files/rofi/arthur.rasi;
    "rofi/change-layout".source = ./files/rofi/change-layout;
  };

  gtk =
    let
      extra = {
        extraConfig = {
          gtk-application-prefer-dark-theme = 1;
        };
      };
    in
    {
      enable = true;
      theme.name = "Adwaita-dark";
      gtk3 = extra;
      gtk4 = extra;
    };

  programs = {
    rofi = {
      enable = true;
      terminal = "alacritty";
      package = pkgs.rofi.override {
        plugins = with pkgs; [
          rofi-emoji
          rofi-calc
        ];
      };
      theme = "~/.config/rofi/arthur.rasi";
      extraConfig = {
        modi = "drun,run,emoji,calc,window,windowcd,keyboardOptions:~/.config/rofi/change-layout";
        show-icons = true;
        icon-theme = "Adwaita";
        window-thumbnail = true;

        kb-remove-char-back = "BackSpace,Shift+BackSpace";
        kb-remove-word-back = "Control+BackSpace";
        kb-remove-to-eol = "Control+Alt+k";
        kb-remove-to-sol = "Control+Alt+u";
        kb-accept-entry = "Control+m,Return,KP_Enter";
        kb-row-left = "Control+Page_Up,Control+Alt+h";
        kb-row-right = "Control+Page_Down,Control+Alt+l";
        kb-row-up = "Up,Control+p,Control+k";
        kb-row-down = "Down,Control+n,Control+j";
        kb-mode-next = "Shift+Right,Control+Tab,Control+l";
        kb-mode-previous = "Shift+Left,Control+Shift+Tab,Control+h";
        kb-mode-complete = "Control+Alt+m";

        terminal = "alacritty";
      };
    };
  };

}
