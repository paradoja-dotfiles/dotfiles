{ pkgs, ... }:

{
  home.packages = with pkgs; [
    # should be per project, but so commonly used for now that added generally
    dhall
    dhall-json
    dhall-yaml
    fx
    lnav
    pv
  ];

  xdg.configFile."nix/nix.conf".text = ''
    experimental-features = nix-command flakes
    substituters = https://cache.nixos.org https://nix-community.cachix.org https://typelevel.cachix.org https://digitallyinduced.cachix.org https://haskell-language-server.cachix.org https://helix.cachix.org
    trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs= typelevel.cachix.org-1:UnD9fMAIpeWfeil1V/xWUZa2g758ZHk8DvGCd/keAkg= digitallyinduced.cachix.org-1:y+wQvrnxQ+PdEsCt91rmvv39qRCYzEgGQaldK26hCKE= haskell-language-server.cachix.org-1:juFfHrwkOxqIOZShtC4YC1uT1bBcq2RSvC7OMKx0Nz8= helix.cachix.org-1:ejp9KQpR1FBI2onstMQ34yogDm4OgU2ru6lIwPvuCVs=
  '';

  xdg.configFile."fish/completions/aws.fish".text = ''
    function __fish_complete_aws
        env COMP_LINE=(commandline -pc) aws_completer | tr -d ' '
    end

    complete -c aws -f -a "(__fish_complete_aws)"
  '';

  programs = {
    awscli = {
      enable = true;
    };
  };
}
