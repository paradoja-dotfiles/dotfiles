{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    (tree-sitter.withPlugins (_: tree-sitter.allGrammars))
    marksman
    python311Packages.grip
  ];

  programs = {
    emacs = {
      enable = true;
      package = pkgs.emacs;
    };

    helix = {
      enable = true;
      extraPackages = [ pkgs.marksman ];
      settings = {
        editor.cursor-shape = { insert = "bar"; };
        editor.auto-format = true;
      };
      languages.language = [{
        name = "cpp";
        auto-format = true;
      }];
    };

    neovim = {
      # it's good to have a couple of colours when a good editor is not around
      enable = true;
      extraPackages = with pkgs; [ shfmt nodejs ];
      coc = {
        enable = true;
        settings = {
          languageserver = {
            bash = {
              command = "bash-language-server";
              args = [ "start" ];
              filetypes = [ "sh" ];
            };
            dhall = {
              command = "dhall-lsp-server";
              filetypes = [ "dhall" ];
            };
            haskell = {
              command = "haskell-language-server-wrapper";
              args = [ "--lsp" ];
              rootPatterns = [
                "*.cabal"
                "stack.yaml"
                "cabal.project"
                "package.yaml"
                "hie.yaml"
              ];
              filetypes = [ "haskell" "lhaskell" ];
            };
            nix = {
              command = "rnix-lsp";
              filetypes = [ "nix" ];
            };
          };
        };
      };
      plugins = with pkgs;
        with vimPlugins; [
          sensible
          vim-nix
          vim-orgmode
          vim-surround

          # tree-sitter plugins
          (nvim-treesitter.withPlugins (plugins: pkgs.tree-sitter.allGrammars))
          rainbow-delimiters-nvim

          # coc
          coc-json
          coc-markdownlint
        ];
      vimAlias = true;
      viAlias = true;
    };
  };

  xdg.dataFile."applications/org-protocol.desktop".text = ''
    [Desktop Entry]
    Name=Org-Protocol
    Exec=~/.nix-profile/bin/emacsclient %u
    Icon=emacs-icon
    Type=Application
    Terminal=false
    MimeType=x-scheme-handler/org-protocol
  '';

  # https://github.com/nix-community/emacs-overlay
  # nixpkgs.overlays = [
  #   (import (builtins.fetchGit {
  #     url = "https://github.com/nix-community/emacs-overlay.git";
  #     ref = "master";
  #     rev =
  #       "456374df242902ba4a749260f218d0e4fa646bc7"; # Emacs - use `porfi dependency-emacs`
  #   }))
  # ];
}
