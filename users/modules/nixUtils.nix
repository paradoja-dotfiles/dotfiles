{ pkgs, nix-index-database, ... }:
# utils to work on nix packages, the system and everything
# except for home manager and the nix command itself
{
  home.packages = with pkgs; [
    cachix
    nix-du
    nix-prefetch-git
    nixfmt-rfc-style # TODO change when new nixfmt is done & everything reformatted?
    nurl
    nvd
  ];
  # nix-du -s=500MB | dot -Tsvg > store.svg

  services.lorri.enable = false; # TODO Remove

  programs = {
    nix-index = {
      enable = true;
    };

    nix-index-database.comma = {
      enable = true;
    };
  };
}
