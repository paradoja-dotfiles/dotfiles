{ config, pkgs, ... }:

{
  xdg.configFile."git/gitignore".source = ./globalGitignore;

  programs = {
    # TODO
    # gh = {
    #   enable = true;
    # };

    git = {
      enable = true;
      userName = "Abby Henríquez Tejera";
      userEmail = "paradoja@gmail.com";
      includes = [ { path = "config-local"; } ];

      delta = {
        enable = true;
        options = {
          features = "line-numbers decorations";
          whitespace-error-style = "22 reverse";
          decorations = {
            commit-decoration-style = "bold yellow box ul";
            file-style = "bold yellow ul";
            file-decoration-style = "none";
          };
        };
      };

      extraConfig = {
        core = {
          excludesfile = "~/.config/git/gitignore";
          # gitignore included in home.files
        };
        github = {
          user = "paradoja";
        };
        gitlab = {
          user = "paradoja";
        };
        init = {
          defaultBranch = "main";
        };
      };
    };
  };
}
