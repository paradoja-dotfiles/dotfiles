{ ... }:
{
  home.file = {
    "bin" = {
      source = ./files/bin;
      recursive = true;
    };

    ".config/fish/completions/porfi.fish".source = ./files/fish-completions/porfi.fish;
    ".local/share/applications/emacsclientC.desktop".source = ./files/desktop-applications/emacsclientC.desktop;
  };
}
