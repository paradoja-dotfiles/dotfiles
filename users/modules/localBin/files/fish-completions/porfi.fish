#!/usr/bin/env fish
# porfi fish completions

# TODO Automate?
# set -l commands apply-system apply-users dist-upgrade sync-system update-deps
complete -c porfi -f
complete -c porfi -a apply-system -d "Apply NixOS system changes"
complete -c porfi -a apply-users -d "Apply home-manager changes"
complete -c porfi -a dist-upgrade -d "Super upgrade of everything nix-related"
complete -c porfi -a sync-system -d "Sync with latest state"
complete -c porfi -a update-deps -d "Update nix flake deps"
complete -c porfi -a system-setup -d "Set up system - standard conf."
complete -c porfi -a clean -d "Clean disk a bit"
