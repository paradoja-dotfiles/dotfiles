{ pkgs, ... }:
{
  home.packages = with pkgs; [
    # should be per project, but so commonly used for now that added generally
    dig
    sshpass
    whois
  ];
}
