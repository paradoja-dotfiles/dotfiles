{
  description = "Blulaktuko's dotfiles";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/master";
    home-manager.url = "github:nix-community/home-manager/master";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    nixgl.url = "github:guibou/nixGL";
    nix-index-database.url = "github:nix-community/nix-index-database";
    nix-index-database.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs =
    {
      nixpkgs,
      home-manager,
      nixos-hardware,
      nixgl,
      nix-index-database,
      ...
    }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;

        overlays = [ nixgl.overlay ];
        config = {
          allowUnfree = false;
          allowUnfreePredicate =
            pkg:
            builtins.elem (lib.getName pkg) [
              "vscode"
            ];
        };
      };
      lib = nixpkgs.lib;
    in
    {
      homeManagerConfigurations =
        let
          username = "paradoja";
          homeDirectory = "/home/paradoja";
          stateVersion = "24.11";
        in
        {
          paradoja = home-manager.lib.homeManagerConfiguration {
            inherit pkgs;
            modules = [
              ./users/paradoja/home.nix
              ./users/modules/desktop
              {
                home = {
                  inherit username homeDirectory stateVersion;
                };
              }
              nix-index-database.hmModules.nix-index
            ];
          };

          paradojaNonDesktop = home-manager.lib.homeManagerConfiguration {
            inherit
              system
              pkgs
              username
              homeDirectory
              stateVersion
              ;
            configuration = {
              imports = [ ./users/paradoja/home.nix ];
            };
          };
        };
      nixosConfigurations = {
        campanota = lib.nixosSystem {
          inherit system pkgs;
          modules = [
            ./system/hosts/campanota
            ./system/modules/gaming.nix
            nixos-hardware.nixosModules.dell-xps-13-9360
          ];
        };
        poseidon = lib.nixosSystem {
          inherit system pkgs;
          modules = [ ./system/hosts/poseidon ];
        };
      };

      devShell.x86_64-linux = pkgs.mkShell {
        buildInputs = with pkgs; [
          nodejs # bash-ls needs it
          nodePackages.bash-language-server
          shellcheck
          nixfmt-rfc-style
          nix-prefetch-git
          nil

          # for update scripts
          gnused # probably in the system anyway
          jq
          curl
        ];
      };
    };
}
