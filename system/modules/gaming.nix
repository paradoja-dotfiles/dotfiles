{ config, lib, pkgs, ... }:

{
  programs.steam.enable = true;
  services.flatpak.enable = true;
}
